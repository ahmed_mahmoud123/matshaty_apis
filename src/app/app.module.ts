import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {RouterModule ,Routes } from '@angular/router'
import { AppComponent } from './app.component';
import { AddTeamComponent } from './components/add-team/add-team.component';
import { ListTeamsComponent } from './components/list-teams/list-teams.component';
import { AddMatchComponent } from './components/add-match/add-match.component';
import { ListMatchesComponent } from './components/list-matches/list-matches.component';
import { EditMatchComponent } from './components/edit-match/edit-match.component';
import { AddNewComponent } from './components/add-new/add-new.component';
import { ListNewsComponent } from './components/list-news/list-news.component';
import { Ng2CompleterModule } from "ng2-completer";

import { ImageUploadModule } from 'angular2-image-upload';
const routes:Routes=[
  {
    path:'',
    redirectTo:"/add_match",
    pathMatch:"full"
  },
  {
    path:"add_team",
    component:AddTeamComponent
  },
  {
    path:"list_teams",
    component:ListTeamsComponent
  },
  {
    path:"add_match",
    component:AddMatchComponent
  },
  {
    path:"list_matchs",
    component:ListMatchesComponent
  },
  {
    path:"edit_match",
    component:EditMatchComponent
  },
  {
    path:"add_new",
    component:AddNewComponent
  },
  {
    path:"list_news",
    component:ListNewsComponent
  }
]
@NgModule({
  declarations: [
    AppComponent,
    AddTeamComponent,
    ListTeamsComponent,
    AddMatchComponent,
    ListMatchesComponent,
    EditMatchComponent,
    AddNewComponent,
    ListNewsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(routes),
    ImageUploadModule.forRoot(),
    Ng2CompleterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
