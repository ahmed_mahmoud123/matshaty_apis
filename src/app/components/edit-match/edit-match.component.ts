import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-match',
  templateUrl: './edit-match.component.html',
  styleUrls: ['./edit-match.component.css']
})
export class EditMatchComponent implements OnInit {

  match:any={};
  constructor(public route:ActivatedRoute) {

  }

  ngOnInit() {
    this.route
      .queryParams
      .subscribe(params => {
          let match = params['match'];
          this.match=JSON.parse(match);
      });
  }

}
