import { Component, OnInit } from '@angular/core';
import { TeamService } from '../../providers/team.service';

@Component({
  selector: 'app-list-teams',
  templateUrl: './list-teams.component.html',
  styleUrls: ['./list-teams.component.css'],
  providers:[TeamService]
})
export class ListTeamsComponent implements OnInit {

  teams:Array<any>=[];
  constructor(public teamService:TeamService) { }

  ngOnInit() {
    this.teamService.list().then(teams=>{
      this.teams=teams;
    })
  }

  delete(id){
    this.teamService.delete(id).then(teams=>{
      this.teams=teams;
    })
  }

}
