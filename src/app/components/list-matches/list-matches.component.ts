import { Component, OnInit } from '@angular/core';
import { MatchService } from '../../providers/match.service';
import { CrawlService } from '../../providers/crawl.service';

import { Router } from '@angular/router';

@Component({
  selector: 'app-list-matches',
  templateUrl: './list-matches.component.html',
  styleUrls: ['./list-matches.component.css'],
  providers:[MatchService,CrawlService]

})
export class ListMatchesComponent implements OnInit {

  date_str:string;
  matches:Array<any>=[];
  result:string;
  constructor(public crawlService:CrawlService,public router:Router,public matchService:MatchService) {
    let today=new Date();
    this.date_str=`${today.getDate()}/${today.getMonth()+1}/${today.getFullYear()}`;
  }

  ngOnInit() {
    this.search();
  }

  delete(id){
    this.matchService.delete(id).then(matches=>{
      this.search();
    })
  }

  edit(match){
    this.router.navigate(['/edit_match'],{queryParams:{match:JSON.stringify(match)}})
  }

  search(){
    this.matchService.list(this.date_str).then(matches=>{
      this.matches=matches.map(match=>{
        match.datetime=new Date(match.datetime);
        return match;
      });
    })
  }

  withdraw(){
    this.result="انتظر يتم السحب الان ....";
    this.crawlService.get(this.date_str).then(matches=>{
      this.result=` Matches ${matches.length} `;
    });
  }

}
