import { Component, OnInit } from '@angular/core';
import { TeamService } from '../../providers/team.service';
@Component({
  selector: 'app-add-team',
  templateUrl: './add-team.component.html',
  styleUrls: ['./add-team.component.css'],
  providers:[TeamService]
})
export class AddTeamComponent implements OnInit {

  team:any={};
  constructor(public teamService:TeamService) { }

  ngOnInit() {
  }

  imageUploaded(event){
    this.team.image=event.src;
  }

  save(){
    this.teamService.add(this.team).then(result=>{
      alert('done')
    });
  }
}
