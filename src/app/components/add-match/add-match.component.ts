import { Component, OnInit,Input } from '@angular/core';
import { TeamService } from '../../providers/team.service';
import { MatchService } from '../../providers/match.service';
import { CompleterService, CompleterData } from 'ng2-completer';


declare var $:any;
@Component({
  selector: 'app-add-match',
  templateUrl: './add-match.component.html',
  styleUrls: ['./add-match.component.css'],
  providers:[TeamService,MatchService]
})
export class AddMatchComponent implements OnInit {

  public dataService: CompleterData;
  public champService: CompleterData;
  public captain_home: string;
  public captain_away: string;
  public captain_champ: string;
  @Input('match') match:any={link_type:1};
  teams:Array<any>=[];
  home_log="";
  away_log="";
  old_lik:string;
  constructor(public completerService: CompleterService,public teamService:TeamService,public matchService:MatchService) {
    this.dataService = this.completerService.remote("http://127.0.0.1:5000/teams/search/", 'name', 'name');
    this.champService = this.completerService.remote("http://127.0.0.1:5000/champ/search/", 'name', 'name');

  }

  ngOnInit() {
    let self=this;
    if(this.match.datetime){
      this.old_lik=this.match.link;
      let d=new Date(this.match.datetime);
      let d_str=`${d.getFullYear()}-${d.getMonth()+1}-${d.getDate()} ${d.getHours()}:${d.getMinutes()}`
      $('#datetime').val(d_str);
      this.match.datetime=+d;
      this.captain_away=this.match.away.name;
      this.away_log=this.match.away.image
      this.home_log=this.match.home.image
      this.captain_home=this.match.home.name;
      this.captain_champ=this.match.championship.name;
    }

    $('#datetime').datetimepicker({
      format: 'yyyy-mm-dd hh:ii',
      autoclose: true,
      todayBtn: true,
    });

    $('#datetime').on('change',function(){
      self.match.datetime=+new Date($(this).val());
    });

  }

  item_select(event,team){
    if(event){
      if(team==0){
        this.match.home=event.originalObject._id;
        this.home_log=event.originalObject.image;
      }else if (team==1){
        this.match.away=event.originalObject._id;
        this.away_log=event.originalObject.image;
      }
    }
  }

  champ_select(event){
    if(event){
        this.match.championship=event.originalObject._id;
    }
  }

  save(){
    this.match.teams=[this.match.home,this.match.away];
    this.match.home_name=this.captain_home;
    this.match.away_name=this.captain_away;
    let now=+new Date();
    let d=new Date(this.match.datetime);
    this.match.date_str=`${d.getDate()}/${d.getMonth()+1}/${d.getFullYear()}`;
    this.match.mintuesHours=`${d.getHours()<10?"0"+d.getHours():d.getHours()}:${d.getMinutes()<10?"0"+d.getMinutes():d.getMinutes()}`;
    let after90=+new Date(this.match.datetime+(90*60*1000));
    if(now >= (+d)){
      this.match.status=1;
    }else{
      this.match.status=2;
    }
    if(now >= after90){
      this.match.status=0;
    }

    if(this.match._id){
      if(this.old_lik != this.match.link)
        this.match.link_change=true;
      else
        this.match.link_change=false;
      this.matchService.edit(this.match).then(match=>{
        alert('done')
      });
    }else{
      this.matchService.add(this.match).then(match=>{
        alert('done')
      });
    }
  }

}
