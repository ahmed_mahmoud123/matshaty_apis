import { Component, OnInit } from '@angular/core';
import {NewsService} from '../../providers/news.service';
@Component({
  selector: 'app-add-new',
  templateUrl: './add-new.component.html',
  styleUrls: ['./add-new.component.css'],
  providers:[NewsService]
})
export class AddNewComponent implements OnInit {
  _new:any={};
  constructor(public newsService:NewsService) { }

  ngOnInit() {
  }

  imageUploaded(event){
    this._new.image=event.src;
  }

  save(){
    this.newsService.add(this._new).then(result=>{
      alert('done');
    })
  }

}
