import { Component, OnInit } from '@angular/core';
import {NewsService} from '../../providers/news.service';

@Component({
  selector: 'app-list-news',
  templateUrl: './list-news.component.html',
  styleUrls: ['./list-news.component.css'],
  providers:[NewsService]
})

export class ListNewsComponent implements OnInit {

  _news:Array<any>=[];
  constructor(public newsService:NewsService) { }

  ngOnInit() {
    this.newsService.list().then(_news=>{
      this._news=_news.docs;
    })
  }

  delete(id){
    this.newsService.delete(id).then(_news=>{
      this._news=_news;
    })
  }

}
