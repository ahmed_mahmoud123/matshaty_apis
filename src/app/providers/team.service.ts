import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class TeamService {
  headers:Headers;
  api_url:string;
  constructor(public http: Http) {
    this.api_url="http://198.12.156.161:5000/teams";
    this.headers = new Headers ({ 'Content-Type': 'application/json' });
  }

  list(){
    return this.http.get(`${this.api_url}/list/`,{headers:this.headers}).map(resp=>resp.json()).toPromise();
  }

  add(team){
    return this.http.post(`${this.api_url}/new`,JSON.stringify(team),{headers:this.headers}).map(resp=>resp.json()).toPromise();
  }

  delete(id){
    return this.http.get(`${this.api_url}/delete/${id}`,{headers:this.headers}).map(resp=>resp.json()).toPromise();
  }
}
