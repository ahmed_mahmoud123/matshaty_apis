import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class CrawlService {
  headers:Headers;
  api_url:string;
  constructor(public http: Http) {
    this.api_url="http://198.12.156.161:5000/crawl";
    this.headers = new Headers ({ 'Content-Type': 'application/json' });
  }

  get(date){
    return this.http.get(`${this.api_url}/${date}`,{headers:this.headers}).map(resp=>resp.json()).toPromise();
  }

}
