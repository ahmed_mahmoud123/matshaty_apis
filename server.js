var express=require('express');
var mongoose=require('mongoose');
var http=require('http');
var fs=require("fs");
const config=JSON.parse(fs.readFileSync(`${__dirname}/env.json`));
var app=express(server);
var server=http.createServer(app);
app.use(express.static('dist'));
app.use(express.static('server/uploads'));

mongoose.connect(config.development.DB_URL);


// Loading Models
fs.readdirSync("./server/models/").forEach(file=>{
  require(`./server/models/${file}`);
});

let sockets=require('./server/utility/realtime');
sockets.realtime(server);


app.use(function (req, res, next) {
    //res.header('transfer-encoding', 'chunked');
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    req.sockets=sockets.users;
    next();
});

var job=require('./server/utility/cronJob');
job.match_status(sockets.users);
var result=require('./server/utility/cronJob_result');
result.update_results(sockets.users);
require('./server/utility/cronJob_news').get_news();
require('./server/utility/cronJob_videos').get_videos();
require('./server/utility/cronJob_matches').get_matches();
// Loading Routes
fs.readdirSync("./server/routes/").forEach(file=>{
    let route=require(`./server/routes/${file}`);
    app.use(`/${route.route_name}`,route);
});



app.get('/*',(req,resp)=>{
  resp.sendFile(__dirname+"/dist/index.html");
});



server.listen(process.env.PORT || 5000);
