var mongoose=require('mongoose');
//var findOneOrCreate = require('mongoose-find-one-or-create');
var mongoosePaginate = require('mongoose-paginate');
class Comment{

  constructor(){
    this.init();
  }

  init(){
    var Schema=mongoose.Schema;
    var comment=new Schema({
      from:String,
      to:String,
      likes:[String],
      text:String,
      datetime:String
    });
    comment.plugin(mongoosePaginate);
    mongoose.model("comments",comment);
  }
}

module.exports=new Comment();
