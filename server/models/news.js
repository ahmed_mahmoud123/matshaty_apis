var mongoose=require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

class News{

  constructor(){
    this.init();
  }

  init(){
    var Schema=mongoose.Schema;
    var _new=new Schema({
      title:String,
      image:String,
      datetime:{
        default:new Date,
        type:Date
      },
      link:String
    });
    _new.plugin(mongoosePaginate);
    mongoose.model("news",_new);
  }
}

module.exports=new News();
