var mongoose=require('mongoose');
var findOneOrCreate = require('mongoose-find-one-or-create');

class Match{

  constructor(){
    this.init();
  }

  init(){
    var Schema=mongoose.Schema;
    var match=new Schema({
      home:String,
      away:String,
      championship:String,
      datetime:{
        type:Number,
        default:+new Date()
      },
      link:String,
      date_str:String,
      teams:[String],
      status:{
        type:Number,
        default:2
      },
      result:{
        type:String,
        default:"0-0"
      },
      mintuesHours:String,
      link_change:{
        type:Boolean,
        default:false
      },
      link_type:Number
    });
    match.plugin(findOneOrCreate);
    mongoose.model("matches",match);
  }
}

module.exports=new Match();
