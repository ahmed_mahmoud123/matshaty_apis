var mongoose=require('mongoose');
var findOneOrCreate = require('mongoose-find-one-or-create');

class YallaMatches{

  constructor(){
    this.init();
  }

  init(){
    var Schema=mongoose.Schema;
    var match=new Schema({
      championship:String,
      team_1:String,
      team_2:String,
      date_str:String,
      time:String,
      result:String,
      datetime:Number
    });

    match.virtual('champ_match', {
    ref: 'championships', // The model to use
    localField: 'championship', // Find people where `localField`
    foreignField: 'name', // is equal to `foreignField`
    });
    match.plugin(findOneOrCreate);
    mongoose.model("yalla_matches",match);
  }
}

module.exports=new YallaMatches();
