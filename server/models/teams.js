var mongoose=require('mongoose');
var findOneOrCreate = require('mongoose-find-one-or-create');

class Team{

  constructor(){
    this.init();
  }

  init(){
    var Schema=mongoose.Schema;
    var team=new Schema({
      name:String,
      image:String,
      cats:[String]
    });
    team.plugin(findOneOrCreate);
    mongoose.model("teams",team);
  }
}

module.exports=new Team();
