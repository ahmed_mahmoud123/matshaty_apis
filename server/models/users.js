var mongoose=require('mongoose');
var findOneOrCreate = require('mongoose-find-one-or-create');

class Users{

  constructor(){
    this.init();
  }

  init(){
    var Schema=mongoose.Schema;
    var user=new Schema({
      _id:String,
      name:String,
      avatar:String,
      email:String,
      token:String,
      teams:[String],
      socket:String
    });
    user.plugin(findOneOrCreate);
    mongoose.model("users",user);
  }
}

module.exports=new Users();
