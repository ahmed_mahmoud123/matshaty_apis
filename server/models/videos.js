var mongoose=require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

class Videos{

  constructor(){
    this.init();
  }

  init(){
    var Schema=mongoose.Schema;
    var _video=new Schema({
      title:String,
      datetime:{
        default:new Date,
        type:Date
      },
      link:String
    });
    _video.plugin(mongoosePaginate);
    mongoose.model("videos",_video);
  }
}

module.exports=new Videos();
