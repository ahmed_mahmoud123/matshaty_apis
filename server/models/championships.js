var mongoose=require('mongoose');
var findOneOrCreate = require('mongoose-find-one-or-create');

class Championship{

  constructor(){
    this.init();
  }

  init(){
    var Schema=mongoose.Schema;
    var championship=new Schema({
      name:String
    });
    championship.plugin(findOneOrCreate);
    mongoose.model("championships",championship);
  }
}

module.exports=new Championship();
