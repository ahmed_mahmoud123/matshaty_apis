var express=require('express');
var bodyParser=require('body-parser');
let middleware=bodyParser.json({limit: '50mb'});
var fs=require('fs');
var thumb = require('node-thumbnail').thumb;
let mongoose=require('mongoose');
let teamModel=mongoose.model('teams');
let router=express.Router();

router.post("/new",middleware,(req,resp)=>{
  let image_name=+new Date();
  let image=req.body.image;
  base64Data  =   image.replace(/^data:image\/jpeg;base64,/, "");
  base64Data  =   base64Data.replace(/^data:image\/png;base64,/, "");
  base64Data  +=  base64Data.replace('+', ' ');
  binaryData  =   new Buffer(base64Data, 'base64').toString('binary');
  fs.writeFileSync(__dirname+'/../uploads/'+image_name+".png",binaryData,"binary");
  thumb({
    source: __dirname+'/../uploads/'+image_name+".png",
    destination: __dirname+'/../uploads/',
    width: 150,
  }, function(files, err, stdout, stderr) {});
  req.body.image=image_name+"_thumb.png";
  teamModel.findOneOrCreate({name:req.body.name},req.body,(err,team)=>{
    if(!err)
      resp.json(team);
    else
      resp.json(err);
  })
});

router.get("/list",middleware,(req,resp)=>{
  teamModel.find({},(err,teams)=>{
    if(!err)
      resp.json(teams);
    else
      resp.json(err);
  })
});

router.get("/search/:keyword?",middleware,(req,resp)=>{
  let result;
  if(req.params.keyword)
    result=teamModel.find({name:{'$regex':`.*${req.params.keyword}.*`, '$options' : 'i'}});
  else
    result=teamModel.find({});
  result.then(teams=>{
      resp.json(teams);
  })
});

router.get("/delete/:id",middleware,(req,resp)=>{
  teamModel.remove({_id:req.params.id},(err,result)=>{
    teamModel.find({},(err,teams)=>{
      if(!err)
        resp.json(teams);
      else
        resp.json(err);
    })
  })
});

router.route_name="teams";
module.exports=router;
