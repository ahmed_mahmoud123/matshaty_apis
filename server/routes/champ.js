var express=require('express');
var notification=require('../utility/notification');
let mongoose=require('mongoose');
let matchModel=mongoose.model('matches');
let teamsModel=mongoose.model('teams');
let userModel=mongoose.model('users');
let champModel=mongoose.model('championships');

let router=express.Router();


router.get("/list",(req,resp)=>{
  champModel.find({}).sort({name:1}).exec(function(err,champs){
    if(!err)
      resp.json(champs);
    else
      resp.json(err);
  })
});


router.get("/teams/:champ_id",(req,resp)=>{
  teamsModel.find({cats:req.params.champ_id},(err,teams)=>{
    if(!err)
      resp.json(teams);
    else
      resp.json(err);
  })
});

router.get("/search/:keyword?",(req,resp)=>{
  let result;
  if(req.params.keyword)
    result=champModel.find({name:{'$regex':`.*${req.params.keyword}.*`, '$options' : 'i'}});
  else
    result=champModel.find({});
  result.then(teams=>{
      resp.json(teams);
  })
});

router.route_name="champ";
module.exports=router;
