var express = require('express');
var request = require('sync-request');
var middleware=require('body-parser').json();
var cheerio = require('cheerio');
var validator=require('validator');
var mongoose=require('mongoose');
let matchModel=mongoose.model('matches');
let teamsModel=mongoose.model('teams');
let champModel=mongoose.model('championships');
let userModel=mongoose.model('users');
var notifications=require('../utility/notification');

let yalla_matches_model=mongoose.model('yalla_matches');
let router=express.Router();



router.get("/:date/:month/:year",middleware,(req,resp)=>{
  let date=new Date(`${req.params.month}/${req.params.date}/${req.params.year}`);
  let now=new Date();
  let temp_date=undefined;
  now.setDate(now.getDate()+1);
  matches=[];
  while(now >= date) {
    let match={};
    date=new Date(date.setDate(date.getDate()+1));
    let d_month=(date.getMonth()+1)<10?"0"+(date.getMonth()+1):(date.getMonth()+1);
    let d_day=date.getDate()<10?"0"+date.getDate():date.getDate();
    url = `http://www.yallakora.com/ar/Matches?gmtvalue=1&selecteddate=${date.getDate()}/${date.getMonth()+1}/${date.getFullYear()}#MatchesClip`;
    console.log(url);
    championships=[];
    let response=request('GET',url);
    console.log(url);
    let body=response.getBody();
    var $ = cheerio.load(body);
    list=$('.MatchList');
    list.find('.ClipTitles .TitleTxt').each(function(){
      championships.push($(this).text());

    })
    list.find('.ClipTitles').siblings('ul').each(function(i){
      $(this).find('.MatchClipData').each(function(index){
        match={championship:validator.trim(championships[i])};
        match['time']=validator.trim($(this).find('.MatchTime').text());
        match['result']=validator.trim($(this).find('.MatchResult .Nums').text());
        let img_link=$(this).find('.TeamOne img.TeamLogoSmall').attr('onerror');
        match["team_1_img"]=img_link.substring(28, img_link.length-2);
        img_link=$(this).find('.TeamTwo img.TeamLogoSmall').attr('onerror');
        match["team_2_img"]=img_link.substring(28, img_link.length-2);
        match.date_str=`${date.getDate()}/${date.getMonth()+1}/${date.getFullYear()}`;
        temp_date=new Date(`${date.getFullYear()}-${d_month}-${d_day} ${match.time}:00`);
        temp_date.setHours(temp_date.getHours()-10);
        match.datetime=+temp_date;
        $(this).find('.TeamName').each(function(index){
          match[`team_${index+1}`]=validator.trim($(this).text());
        });
        matches.push(match);
      });
    });

  }
  //resp.json(matches);

  function setTeamOne(matches){
    console.log(matches.length);
    console.log("setTeamOne");
    let new_match=[];
    matches.forEach((match,i)=>{
      (function(i){
        teamsModel.findOneOrCreate({name:match.team_1},{name:match.team_1,image:match.team_1_img},(e,m)=>{
          match.team_1=m._id;
          new_match.push(match);
          if(new_match.length==matches.length)
            setTeamTwo(new_match);
        });
      })(i);
    });
  }

  function setTeamTwo(matches){
    console.log("setTeamTwo");
    let new_match=[];
    matches.forEach((match,i)=>{
      (function(i){
        teamsModel.findOneOrCreate({name:match.team_2},{name:match.team_2,image:match.team_2_img},(e,m)=>{
          match.team_2=m._id;
          new_match.push(match);
          if(new_match.length==matches.length)
            setTeamChamp(new_match);
        });
       })(i);
    });
  }

  function setTeamChamp(matches){
    console.log('setTeamChamp');
    let new_match=[];
    matches.forEach((match,i)=>{
      (function(i){
        champModel.findOneOrCreate({name:match.championship},{name:match.championship},(e,champ)=>{
          match.championship=champ._id;
          new_match.push(match);
          if(new_match.length==matches.length){
            saveOnDB(new_match);
            resp.json(matches);
          }
        });
      })(i);
    });
  }

  function saveOnDB(matches){
    matches=matches.map(match=>{
      delete match.team_1_img;
      delete match.team_2_img;
      match.teams=[match.team_1,match.team_2];
      var str = JSON.stringify(match);
      str = str.replace(/team_1/g, 'home');
      str = str.replace(/team_2/g, 'away');
      str = str.replace(/time/g, 'mintuesHours');
      str = str.replace(/datemintuesHours/g, 'datetime');
      return JSON.parse(str);
    });
    //console.log(matches);
    matchModel.collection.insert(matches,{ordered:false},(err,rs)=>{
      console.log(err);
      //sendNotifications(matches);
    });
  }

  function sendNotifications(matches){
    teamsModel.populate(matches,{path:"home away",select:"name"},(err,matches)=>{
      let i=0;
      matches.forEach(match=>{
        userModel.find({teams:{$in:[match.home._id,match.away._id]}},{token:true},(err,users)=>{
          if(users)
            users.forEach((user)=>{
              notifications.send(` ${match.home.name} - ${match.away.name} `,` ${match.date_str} ${match.mintuesHours} `,user.token,++i);
            });
        });
      });
    });
  }

  setTeamOne(matches);
});

router.route_name="crawl";
module.exports=router;
