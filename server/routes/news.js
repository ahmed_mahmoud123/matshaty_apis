var express=require('express');
var fs=require('fs');
var thumb = require('node-thumbnail').thumb;
var mongoosePaginate = require('mongoose-paginate');
var bodyParser=require('body-parser');
let middleware=bodyParser.json({limit: '50mb'});
let mongoose=require('mongoose');
let newModel=mongoose.model('news');
let router=express.Router();

router.post("/new",middleware,(req,resp)=>{
  let _new=new newModel(req.body);
  let image_name=+new Date();
  let image=req.body.image;
  base64Data  =   image.replace(/^data:image\/jpeg;base64,/, "");
  base64Data  =   base64Data.replace(/^data:image\/png;base64,/, "");
  base64Data  +=  base64Data.replace('+', ' ');
  binaryData  =   new Buffer(base64Data, 'base64').toString('binary');
  fs.writeFileSync(__dirname+'/../uploads/'+image_name+".png",binaryData,"binary");
  thumb({
    source: __dirname+'/../uploads/'+image_name+".png",
    destination: __dirname+'/../uploads/',
    width: 150,
  }, function(files, err, stdout, stderr) {});
  _new.image=image_name+"_thumb.png";
  _new.save((err,_new)=>{
    if(!err)
      resp.json(_new);
    else
      resp.json(err);
  })
});

router.get("/list/:page/:keyword?",(req,resp)=>{
  let page=req.params.page?req.params.page:1;
  let _new;
  if(req.params.keyword)
    _new=newModel.paginate({title:{'$regex':`.*${req.params.keyword}.*`, '$options' : 'i'}},{ sort:{datetime:-1},page: page,limit: 20});
  else
    _new=newModel.paginate({},{ sort:{datetime:-1},page: page,limit: 20});
  _new.then((err,__news)=>{
    if(!err)
      resp.json(__news);
    else
      resp.json(err);
  })
});

router.get("/delete/:id",(req,resp)=>{
  newModel.remove({_id:req.params.id},(err,result)=>{
    newModel.find({},(err,teams)=>{
      if(!err)
        resp.json(teams);
      else
        resp.json(err);
    })
  })
});

router.route_name="news";
module.exports=router;
