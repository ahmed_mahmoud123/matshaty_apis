var express=require('express');
var notification=require('../utility/notification');
var bodyParser=require('body-parser');
let middleware=bodyParser.json();
let mongoose=require('mongoose');
let matchModel=mongoose.model('matches');
let teamsModel=mongoose.model('teams');
let userModel=mongoose.model('users');
let champModel=mongoose.model('championships');
let commentModel=mongoose.model('comments');

let router=express.Router();

router.post("/new",middleware,(req,resp)=>{
  // Send notifications and realtime updates ....
  userModel.find({teams:{$in:req.body.teams}},{token:true},(err,users)=>{
    if(!err){
      users.forEach(user=>{
        if(req.sockets[user._id])
          req.sockets[user._id].emit('new_event',{event_name:'new_match',match:req.body});
        if(user.token)
          notification.send("مباره جديده",`${req.body.home_name}-${req.body.away_name} ${req.body.date_str} ${req.body.mintuesHours}`,user.token);
      });
    }
  });
  let match=new matchModel(req.body);
  match.save((err,match)=>{
    if(!err)
      resp.json(match);
    else
      resp.json(err);
  })
});

router.post("/edit",middleware,(req,resp)=>{
  /*userModel.find({teams:{$in:req.body.teams}},{token:true},(err,users)=>{
    if(!err){
      users.forEach(user=>{
        if(req.sockets[user._id]){
          req.sockets[user._id].emit('new_event',{event_name:'edit_match',match:req.body});
        }
      });
    }
  });*/
  Object.keys(req.sockets).forEach(socket=>{
    console.log(socket);
    req.sockets[socket].emit('new_event',{event_name:'edit_match',match:req.body});
  });
  matchModel.update({_id:req.body._id},{'$set':req.body},(err,match)=>{
    if(!err)
      resp.json(match);
    else
      resp.json(err);
  })
});

router.get("/list/:date/:month/:year",(req,resp)=>{
  let date_str=`${req.params.date}/${req.params.month}/${req.params.year}`;
  matchModel.find({date_str:date_str},(err,matches)=>{
    teamsModel.populate(matches,{path:"away home",select:"name image"},(err,matches)=>{
      champModel.populate(matches,{path:"championship",select:"name"},(err,matches)=>{
      if(!err)
        resp.json(matches);
      else
        resp.json(err);
      })
    })
  })
});


router.post("/get",middleware,(req,resp)=>{
  let date_str=req.body.day;
  //matchModel.find({$and:[{$or:[{home:{'$in':req.body.teams}},{away:{'$in':req.body.teams}}]},{date_str:date_str}]},(err,matches)=>{
  console.log(date_str);
  matchModel.find({date_str:date_str}).sort({datetime:1}).exec(function(err,matches){
    teamsModel.populate(matches,{path:"away home",select:"name image"},(err,matches)=>{
      champModel.populate(matches,{path:"championship",select:"name"},(err,matches)=>{
        console.log(matches);
        if(!err)
          resp.json(matches);
        else
          resp.json(err);
      })
    })
  })
});

router.get("/delete/:id",(req,resp)=>{
  matchModel.remove({_id:req.params.id},(err,result)=>{
    if(!err)
      resp.json(result);
    else
      resp.json(err);
  });
});

router.get("/comments/:match_id/:page?",(req,resp)=>{
  let page=req.params.page;
  console.log(page+"  Page ");
  commentModel.paginate({to:req.params.match_id},{ sort:{_id:-1},page: page,limit: 20}).then((comments)=>{
    let docs=comments.docs
    let c_page=comments.page
    let pages=comments.pages
    userModel.populate(docs,{path:"from",select:"name avatar"},(err,comments)=>{
      if(!err)
        resp.json({docs:docs,page:c_page,pages:pages});
      else
        resp.json(err);
    });
  });
});

router.get("/like_comment/:comment_id/:like/:user_id",(req,resp)=>{
  let operation=undefined;
  if(req.params.like=="true"){
    operation =commentModel.update({_id:req.params.comment_id},{$addToSet:{likes:req.params.user_id}});
  }else{
    operation =commentModel.update({_id:req.params.comment_id},{$pull:{likes:req.params.user_id}});
  }
  operation.then(reuslt=>{
    resp.json(reuslt);
  });
});

router.route_name="matches";
module.exports=router;
