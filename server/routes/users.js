var express=require('express');
var bodyParser=require('body-parser');
let middleware=bodyParser.json();
var request=require('request');
var mongoose=require('mongoose');
let userModel=mongoose.model('users');
let router=express.Router();

router.post("/login",middleware,(req,resp)=>{
      userModel.findOne({_id:req.body._id},function(error,user){
      if(!user){
        request(req.body.avatar,(err,response,body)=>{
          //req.body.avatar = "data:" + response.headers["content-type"] + ";base64," + new Buffer(body).toString('base64')
          userModel.findOneOrCreate({_id:req.body._id},req.body,(err,u)=>{
            if(err)
              resp.json({error:true});
            else
              resp.json(u);
          });
        })
      }else{
        resp.json(user);
      }
    });
});

router.post("/teams",middleware,(req,resp)=>{
  userModel.update({_id:req.body._id},{'$addToSet':{teams:{$each:req.body.teams}}},(err,result)=>{
    if(err)
      resp.json(err)
    else
      resp.json(result)
  });
});

router.post("/update_token",middleware,(req,resp)=>{
  console.log("Update Token!!");
  userModel.update({_id:req.body.user_id},{'$set':{token:req.body.token}},(err,result)=>{
    console.log(err);
    if(err)
      resp.json(err)
    else
      resp.json(result)
  });
});

router.route_name="users";
module.exports=router;
