var express=require('express');
var mongoosePaginate = require('mongoose-paginate');
let mongoose=require('mongoose');
let videoModel=mongoose.model('videos');
let router=express.Router();


router.get("/list/:page/:keyword?",(req,resp)=>{
  let page=req.params.page?req.params.page:1;
  let _video;
  if(req.params.keyword)
    _video=videoModel.paginate({title:{'$regex':`.*${req.params.keyword}.*`, '$options' : 'i'}},{ sort:{datetime:-1},page: page,limit: 20});
  else
    _video=videoModel.paginate({},{ sort:{datetime:-1},page: page,limit: 20});
  _video.then((err,__news)=>{
    console.log(__news);
    if(!err)
      resp.json(__news);
    else
      resp.json(err);
  })
});


router.route_name="videos";
module.exports=router;
