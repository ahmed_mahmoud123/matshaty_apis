var request = require('request');
var cheerio = require('cheerio');
var validator=require('validator');
let mongoose=require('mongoose');
let matchModel=mongoose.model('matches');
let teamModel=mongoose.model('teams');
let champModel=mongoose.model('championships');

module.exports=(date)=>{
    var arr=date.split('/');
    arr[1]++;
    url =`http://www.yallakora.com/ar/Matches?gmtvalue=1&selecteddate=${arr[1]}/${arr[0]}/${arr[2]}#MatchesClip`;
    matches=[];
    championships=[];
    matchObj={};
    request(url, function(error, response, html){
        if(!error){
            var $ = cheerio.load(html);
                list=$('.MatchList');
                championships=[];
                let championship=list.find('.ClipTitles .TitleTxt').each(function(){
                  championships.push($(this).text());
                });

                list.find('.ClipTitles').siblings('ul').each(function(i){

                    let match={championship:validator.trim(championships[i])};
                    champModel.findOneOrCreate({name:match.championship},{name:match.championship},(err,champ)=>{
                      //console.log("OBJECT="+i+" "+champ);
                      //console.log(champ._id);
                      setTimeout(syncMethod,1000*(i+1),match,champ._id,$(this),arr);

                    });
                });
            //console.log(matches);
        }


        function syncMethod(match,champ,ulDomObject,arr){

          matchObj.championship=champ;
          ulDomObject.find('.MatchClipData').each(function(index){
            match['time']=validator.trim($(this).find('.MatchTime').text());
            match['result']=validator.trim($(this).find('.MatchResult .Nums').text());
            let img_link=ulDomObject.find('.TeamOne img.TeamLogoSmall').attr('onerror');
            match["team_1_img"]=img_link.substring(28, img_link.length-2);
            img_link=ulDomObject.find('.TeamTwo img.TeamLogoSmall').attr('onerror');
            match["team_2_img"]=img_link.substring(28, img_link.length-2);


            $(this).find('.TeamName').each(function(index){
              match[`team_${index+1}`]=validator.trim($(this).text());
              teamModel.findOneAndUpdate({name:match[`team_${index+1}`]},
                {'$push':{cats:champ},'$set':{name:match[`team_${index+1}`],image:match[`team_${index+1}_img`]}},
              {upsert:true,new: true},(err,team)=>{
                match[`team_${index+1}`]=team._id;
                if((index+1)==2){
                  matchObj.away=match['team_2'];
                  matchObj.home=match['team_1'];
                  matchObj.date_str=date;
                  matchObj.mintuesHours=match['time'];
                  matchObj.result=match['result'];
                  matchObj.datetime=+new Date(`${arr[1]}/${arr[0]}/${arr[2]} ${matchObj.mintuesHours}`);
                  matchObj.teams=[matchObj.away,matchObj.home]
                  matchModel.findOneOrCreate({away:matchObj.away,home:matchObj.home,datetime:matchObj.datetime}
                  ,matchObj,(err,match)=>{console.log(err);});
                }
              });
            });
          });
          matches.push(match);
        }
    })
}
