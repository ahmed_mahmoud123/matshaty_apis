var notifications=require('./notification');
var mongoose=require('mongoose');
let commentModel=mongoose.model('comments');

let users={};
module.exports.realtime=(server)=>{
  var mongoose=require('mongoose');
  let UserModel=mongoose.model('users');
  var io=require('socket.io')(server);
  io.on("connection",client=>{

    client.on('new_event',msg=>{
      console.log("=== NEW MSG ===");
      let comment={from:msg.from,to:msg.to,text:msg.text}
      comment.datetime=+new Date();
      new commentModel(comment).save((err,comment)=>{});
      client.broadcast.emit('new_event',msg);
      client.emit('new_event',msg);
    });

    client.on("join",name=>{
      console.log("new user join"+name);
      client.user_id=name;
      users[name]=client;
    });

    client.on("logout",name=>{
      console.log("Logout"+name);
      delete users[name];
    });

    client.on('disconnect',()=>{
      delete users[client.user_id];
    })
  });
}

module.exports.users=users;
