"use strict"
var schedule = require('node-schedule');
var urlencode = require('urlencode');

var request = require('sync-request');
var cheerio = require('cheerio');
var validator=require('validator');
var mongoose=require('mongoose');
let videoModel=mongoose.model('videos');
var facebook=require('./fb_post');


module.exports.get_videos=()=>{
  var j = schedule.scheduleJob('*/30 * * * *', function(){
    console.log("Video Running !!!");
    let __videos=[];
    let now=+new Date();
    let response=request('GET',"http://filgoal.com/videos");
    let body=response.getBody();
    var $ = cheerio.load(body);
    $('main li').each(function(){
      let video={};
      video.title=$(this).find('h1').text();
      video.datetime=now;
      let page_link=$(this).find('a').attr('href');
      let parts=page_link.split('/');
      parts[parts.length-1]=urlencode(parts[parts.length-1]);
      let video_response=request('GET',"http://filgoal.com"+parts.join('/'));
      var video_$ = cheerio.load(video_response.body.toString());
      video.link=video_$('iframe').attr('src');
      if(video.link && video.link.indexOf('youtube')>=0){
      __videos.push(video)
      }
    });
    //videoModel.collection.insert(__videos,{ordered:false},(err,rs)=>{});
    let kesha=1;
    __videos.forEach(v=>{
      let _v=new videoModel(v);
      _v.save((err,result)=>{
        if(!err)
          setTimeout(function(){facebook.fb_post(result.title+"\n"+result.link)},1000*(++kesha));
      });
    });
  })
}
