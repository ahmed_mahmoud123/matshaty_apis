"use strict"
var schedule = require('node-schedule');
var request = require('sync-request');
var cheerio = require('cheerio');
var validator=require('validator');
var mongoose=require('mongoose');
let matchModel=mongoose.model('matches');
let teamsModel=mongoose.model('teams');
let userModel=mongoose.model('users');
let champModel=mongoose.model('championships');
var notifications=require('./notification');

module.exports.update_results=(sockets)=>{
  var j = schedule.scheduleJob('*/1 * * * *', function(){
    console.log("SOCKETS ARRAY => "+Object.keys(sockets));
    let date=new Date();
    //date+=Number(60*60*1000);
    console.log("Results!!");
    //console.log(new Date());
    date.setHours(date.getHours()+9);
    let date_str=`${date.getDate()}/${date.getMonth()+1}/${date.getFullYear()}`;
    console.log(date_str);
    console.log(date.getHours());
    console.log("ٍResults updating running !!!");
    matchModel.find({date_str:date_str}).sort({datetime:1}).limit(1).exec(function(err,minResult){
      if( minResult[0] && minResult[0].datetime<=date){
        start_crawel();
        /*matchModel.find({date_str:date_str}).sort({datetime:-1}).limit(1).exec(function(err,maxResult){
          if((maxResult[0].datetime+ (210*60*1000)) >= date){
            start_crawel();
          }
        });*/
      }
    });
    function start_crawel(){
        console.log("Start Updating !!!");
        let matches=[];
        let match={};
        let url = `http://www.yallakora.com/ar/Matches?gmtvalue=1&selecteddate=${date.getDate()}/${date.getMonth()+1}/${date.getFullYear()}#MatchesClip`;
        let championships=[];
        let response=request('GET',url);
        let body=response.getBody();
        var $ = cheerio.load(body);
        let list=$('.MatchList');
        list.find('.ClipTitles .TitleTxt').each(function(){
          championships.push($(this).text());

        })
        list.find('.ClipTitles').siblings('ul').each(function(i){
          $(this).find('.MatchClipData').each(function(index){
            match={championship:validator.trim(championships[i])};
            match['time']=validator.trim($(this).find('.MatchTime').text());
            match['result']=validator.trim($(this).find('.MatchResult .Nums').text());
            let img_link=$(this).find('.TeamOne img.TeamLogoSmall').attr('onerror');
            match["team_1_img"]=img_link.substring(28, img_link.length-2);
            img_link=$(this).find('.TeamTwo img.TeamLogoSmall').attr('onerror');
            match["team_2_img"]=img_link.substring(28, img_link.length-2);
            match.date_str=`${date.getDate()}/${date.getMonth()+1}/${date.getFullYear()}`;
            match.datetime=+new Date(`${date.getMonth()+1}/${date.getDate()}/${date.getFullYear()} ${match.time}`);
            $(this).find('.TeamName').each(function(index){
              match[`team_${index+1}`]=validator.trim($(this).text());
            });
            matches.push(match);
          });
        });


        //resp.json(matches);

        function setTeamOne(matches){
          let new_match=[];
          matches.forEach((match,i)=>{
            (function(i){
              teamsModel.findOneOrCreate({name:match.team_1},{name:match.team_1,image:match.team_1_img},(e,m)=>{
                match.team_1=m._id;
                new_match.push(match);
                if(new_match.length==matches.length)
                  setTeamTwo(new_match);
              });
            })(i);
          });
        }

        function setTeamTwo(matches){
          let new_match=[];
          matches.forEach((match,i)=>{
            (function(i){
              teamsModel.findOneOrCreate({name:match.team_2},{name:match.team_2,image:match.team_2_img},(e,m)=>{
                match.team_2=m._id;
                new_match.push(match);
                if(new_match.length==matches.length)
                  setTeamChamp(new_match);
              });
             })(i);
          });
        }

        function setTeamChamp(matches){
          let new_match=[];
          matches.forEach((match,i)=>{
            (function(i){
              champModel.findOneOrCreate({name:match.championship},{name:match.championship},(e,champ)=>{
                match.championship=champ._id;
                new_match.push(match);
                if(new_match.length==matches.length){
                  check_updates(new_match);
                }
              });
            })(i);
          });
        }

        function check_updates(matches){
          matches=matches.map(match=>{
            delete match.team_1_img;
            delete match.team_2_img;
            match.teams=[match.team_1,match.team_2];
            var str = JSON.stringify(match);
            str = str.replace(/team_1/g, 'home');
            str = str.replace(/team_2/g, 'away');
            str = str.replace(/time/g, 'mintuesHours');
            str = str.replace(/datemintuesHours/g, 'datetime');
            return JSON.parse(str);
          });

          matches.forEach((match,i)=>{
            matchModel.update({date_str:date_str,home:match.home,away:match.away},{'$set':{result:match.result,mintuesHours:match.mintuesHours}},(err,rs)=>{
              if(rs.nModified){
                sendNotifications(match)
              }
            });
          });
        }

        function sendNotifications(match){
          matchModel.findOne({home:match.home,away:match.away,date_str:match.date_str},(err,match)=>{
            teamsModel.populate(match,{path:"home away",select:"name"},(err,match)=>{
              match.link_change=false;
              Object.keys(sockets).forEach(socket=>{
                sockets[socket].emit('new_event',{event_name:'edit_match',match:match});
              });

              userModel.find({teams:{$in:[match.home._id,match.away._id]}},{token:true},(err,users)=>{
                if(users)
                  users.forEach((user)=>{
                    notifications.send(match.result,`${match.home.name} - ${match.away.name}`,user.token);
                  });
              });
            });

          })
       }
        setTeamOne(matches);
    }
    //
  })
}
