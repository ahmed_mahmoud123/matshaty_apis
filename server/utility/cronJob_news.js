"use strict"
var schedule = require('node-schedule');
var request = require('sync-request');
var cheerio = require('cheerio');
var validator=require('validator');
var mongoose=require('mongoose');
var newsModel=mongoose.model('news');
var notifications=require('./notification');
var facebook=require('./fb_post');

module.exports.get_news=()=>{
  var j = schedule.scheduleJob('*/30 * * * *', function(){
    let __news=[];
    let now=+new Date();
    let response=request('GET',"http://www.yallakora.com/");
    let body=response.getBody();
    var $ = cheerio.load(body);
    let news=$('.NewsClip');
    news.find('.ClipItem').each(function(){
      let _new={};
      _new.title=$(this).find('.NewsTitle').text();
      _new.link=$(this).find('a').first().attr('href');
      _new.image=$(this).find('.NewsIMGClip img').attr('src');
      _new.datetime=now;
      __news.push(_new);
    });

    let f_response=request('GET',"https://www.filgoal.com/articles");
    let f_body=f_response.getBody();
    var f_$ = cheerio.load(f_body);
    f_$('main li').each(function(){
      let _new={};
      _new.title=$(this).find('h1').text();
      _new.image=$(this).find('img').attr('data-src').replace("//media.filgoal.com/news/large","https://media.filgoal.com/news/small/");
      _new.link="https://www.filgoal.com"+ $(this).find('a').attr('href');
      _new.datetime=now;
      __news.push(_new);
    });

    /*newsModel.collection.insert(__news,{ordered:false},(err,rs)=>{
    });*/
    let kesha=1;
    __news.forEach(n=>{
      let _n=new newsModel(n);
      _n.save((err,result)=>{
        if(!err)
          setTimeout(function(){facebook.fb_post(result.title)},1000*(++kesha));
      });
    });
  })
}
