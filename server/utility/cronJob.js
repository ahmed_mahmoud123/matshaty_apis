"use strict"
var schedule = require('node-schedule');
var mongoose=require('mongoose');
let matchModel=mongoose.model('matches');
let teamModel=mongoose.model('teams');
let userModel=mongoose.model('users');
var notifications=require('./notification');

module.exports.match_status=(sockets)=>{
  var j = schedule.scheduleJob('*/1 * * * *', function(){
    console.log("running !!!");
    let now=+new Date();
    now-=Number(60*60*1000);
    matchModel.find({status:{$ne:0}},(err,matches)=>{
      matches.forEach(match=>{
        let date=new Date(match.datetime+(190*60*1000));
        if(match.datetime <= now && match.status==2){
          matchModel.update({_id:match._id},{'$set':{status:1}},(err,result)=>{
            teamModel.populate(match,{path:"home away",select:"name"},(err,match)=>{
              match.status=1;
              Object.keys(sockets).forEach(socket=>{
                sockets[socket].emit('new_event',{event_name:'match_start',match:match});
              });
              userModel.find({teams:{$in:match.teams}},{token:true},(err,users)=>{
                users.forEach(user=>{
                  notifications.send('مباشر الأن',`${match.home.name}-${match.away.name}`,user.token);
                });
              });
            });
          });
        }else if(match.status==1 && now >= (+date) || match.mintuesHours=='انتهت'){
          let updateObject={status:0};
          if(match.link_type==0)
            updateObject.link=`https://www.facebook.com/plugins/video.php?href=${match.link}`;
          matchModel.update({_id:match._id},{'$set':updateObject},(err,result)=>{
            teamModel.populate(match,{path:"home away",select:"name"},(err,match)=>{
              match.status=0;
              Object.keys(sockets).forEach(socket=>{
                sockets[socket].emit('new_event',{event_name:'match_end',match:match});
              });
              userModel.find({teams:{$in:match.teams}},{token:true},(err,users)=>{
                users.forEach(user=>{
                  notifications.send('انتهت المباره',`${match.home.name}-${match.away.name}  ${match.result}`,user.token);
                });
              });
            });
          });
        }
      });
    })
    //notifications.send("","",);
  })
}
